- 👋 Hi, I’m @xrviv otherwise known as Daniel
- 👀 I’m interested in finding work and changing career to Cybersecurity related 💸💸
- 🌱 I’m currently learning Python, Javascript and Cybersecurity on LinkedIn
- 💞️ I’m looking to collaborate on Open Source python projects
- 📫 How to reach me? 
- 🐦 [@dannybuntu](https://twitter.com/dannybuntu)
- 🌐 https://xrvive.com
- 🦊 [GitLab](https://gitlab.com/dannygarcia)
- 👔 [LinkedIn Profile](https://www.linkedin.com/in/dannyboygarcia/)

<!---
xrviv/xrviv is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
